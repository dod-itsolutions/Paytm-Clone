<h2>Paytm Clone Script</h2>

<i>Recharge =>Shopping =>Bus =>Hotel => Flight =>Cab =>Movie</i>

A popular multivendor website for you, compressing of recharges like Mobile recharge, DTH, Data card, Electricity, Insurance…etc. 
Also provides bookings such as bus bookings, hotel bookings, Flight bookings, Cab bookings and Movie bookings. You can also relish 
your home or personal needs through attractive shopping. Also provides you with a wallet to wallet transfer option and as well QR 
Scanner and generation management

We have skilled developers and designers, with more than 5+ years of experience, who is ready to corporate to client ideas and push their business to the peak. Amazing mob app designs with user-friendly interface provided.

<h2> Features </h2>

♦ Wallet to Wallet Transfer<br>
♦ Wallet to Bank Accont Transfer<br>
♦ QR Code Scanner<br>
♦ Android App<br>
♦ IOS App<br>
♦ Payment Gateway<br>
♦ Email Invoice Generation<br>
♦ SMS Invoice Generation<br>
♦ Web Front Invoice Generation<br>
♦ Passbook Entry<br>
♦ Pay through QR Code<br>
♦ Pay to Mobile Number<br>
♦ Payment Send/Receive Request<br>
♦ Add Beneficiary details<br>
♦ Peer to Peer Payment System<br>
♦ Recharge Portal<br>
♦ Bus Portal<br>
♦ Flight Portal<br>
♦ E-commerce<br>
♦ Withdrawal to bank<br>
♦ Coupon Code Generation<br>
♦ Weekly/Monthly/Yearly Reports<br>
♦ Online Tracking<br>
